# typescript-base

## Setup

1. Install node via NVM

```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash
```

2. Install node LTS

```
nvm ls-remote | grep "Latest LTS"
nvm i <version>
```

3. Install typescript globally

```
npm i -g typescript
```
